See http://ottr.xyz

Download releases: https://gitlab.com/ottr/lutra/lutra/releases

Lastest master snapshot: https://gitlab.com/ottr/lutra/lutra/builds/artifacts/master/raw/lutra.jar?job=snapshot

Lastest develop snapshot: https://gitlab.com/ottr/lutra/lutra/builds/artifacts/develop/raw/lutra.jar?job=snapshot
